# Quickstart to Compute Shaders with OpenGL and Raylib
This simulates a large number of particles on the GPU using a compute shader.
Currently it simulates the Lorenz system. The particles are drawn as tetrahedra.

The camera can be controlled using WASD, CTRL (down) and SPACE (up), and the view can be rotated using the mouse.


Run the simulation by holding T or step the simulation by pressing R.


# Examples
Low framerate examples to keep the gifs small. Looks much better in the actual program.

![result1](examples/result1.gif)

![result2](examples/result2.gif)

# Notes
Raylib has to be build with
```
make PLATFORM=PLATFORM_DESKTOP GRAPHICS=GRAPHICS_API_OPENGL_43
```


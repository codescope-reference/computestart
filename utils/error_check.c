// Helper function to check and print OpenGL errors
void check_error(const char *msg) {
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) {
        printf("Error: %s\n", msg);
        switch (err) {
            case GL_INVALID_ENUM:
                printf("Error: GL_INVALID_ENUM\n");
                break;
            case GL_INVALID_VALUE:
                printf("Error: GL_INVALID_VALUE\n");
                break;
            case GL_INVALID_OPERATION:
                printf("Error: GL_INVALID_OPERATION\n");
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                printf("Error: GL_INVALID_FRAMEBUFFER_OPERATION\n");
                break;
            case GL_OUT_OF_MEMORY:
                printf("Error: GL_OUT_OF_MEMORY\n");
                break;
            case GL_STACK_UNDERFLOW:
                printf("Error: GL_STACK_UNDERFLOW\n");
                break;
            case GL_STACK_OVERFLOW:
                printf("Error: GL_STACK_OVERFLOW\n");
                break;
            default:
                printf("Error: unknown error\n");
                break;
        }
    }
}

#version 430


// Workgroup size
layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
// TODO: Time different sizes

// Position buffer
layout (std430, binding = 0) buffer position
{
    vec4 positions[];
};

// Velocity buffer
layout (std430, binding = 1) buffer velocity
{
    vec4 velocities[];
};

uniform int numParticles;
uniform float dt;

uniform float rho;
uniform float sigma;
uniform float beta;


void main() {
    uint index = gl_GlobalInvocationID.x;
    if (index >= numParticles) {
        return;
    }

    int n_facets = 4;
    for (int i = 0; i < n_facets; i++) {
        for (int j = 0; j < 3; j++) {
            // Important to take position from the same vertex. Otherwise chaotic nature of the system will cause the tetrahedra to blow up.
            vec4 pos = positions[3 * n_facets * index];
            float d_x = dt * (sigma * (pos.y - pos.x));
            float d_y = dt * (pos.x * (rho - pos.z) - pos.y);
            float d_z = dt* (pos.x * pos.y - beta * pos.z);

            vec4 old_pos = positions[3 * n_facets * index + j + i * 3];
            float new_x = old_pos.x + d_x;
            float new_y = old_pos.y + d_y;
            float new_z = old_pos.z + d_z;
            positions[3 * n_facets * index + j + i * 3] = vec4(new_x, new_y, new_z, 0.0f);
        }
    }
}


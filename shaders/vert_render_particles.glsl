#version 430

layout(location = 0) in vec4 position;

uniform mat4 camera;

void main()
{
    gl_Position = camera * vec4(position.xyz, 1.0);
    // gl_Position = vec4(position.xyz, 1.0);
}

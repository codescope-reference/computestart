#include <assert.h>
#include <external/glad.h>
#include <stdio.h>
#include <stdlib.h>
#include "raylib.h"
#include "raymath.h"
#include "rcamera.h"
#include "rlgl.h"
#include "utils/error_check.c"


#define RECT_IMPLEMENTATION
#include "utils/rect.h"

typedef unsigned int u32;

// Simulation parameters
// ---------------------
#define N_PARTICLES 20000
#define N_FACETS 4
// These need to be Vector4 to conform with alignment requirements
// Alternatively, we can be careful when we read the data from GPU to CPU
Vector4 positions[N_PARTICLES * 3 * N_FACETS];
Vector4 velocities[N_PARTICLES];
static float rho = 28.0f;
static float sigma = 10.0f;
static float beta = 2.66f;


#define PARTICLE_SIZE 0.05f


// Global variables for the application
// ------------------------------------
static bool orbit_mode = false;
static Vector3 orbit_target = (Vector3){0.0f, 0.0f, 0.0f};
static float angle_speed = 0.5f;
static bool recording = false;
static float recording_speed = 0.1f;
static float time_since_last_frame = 0.0f;
static float max_dt_for_update = 1.0f / 60.0f;



// Initialize particles
// --------------------
// This sets up a tetrahedron at each particle location.
// The reason for this is that we want to draw a particle with a non-zero size.
// Could also draw spheres, but that obviously requires more triangles.
void init_particles(void) {
    for (int i = 0; i < N_PARTICLES; i++) {
        // We put the particles in a line. 
        // It doesn't really matter, they just need to be a little spread out so we can see the chaotic behaviour of the system.
        float x = 1.0f * (1.0f / (N_PARTICLES - 1) * i) - 0.5f;
        float y = 0.0f;
        float z = 0.0f;
        float d = PARTICLE_SIZE;

       /*
        * Reference drawing of the bottom face of the tetrahedron
        * The top point is above the middle of this triangle
        (x - d, y + d, z)
           *__
           |  \__
           |     \__
           |  *   __* (x, y + d / 2, z)
           |   __/
           |__/
           *
        (x - d, y, z)
       */

        int facet_index = 0;
        positions[3 * N_FACETS * i + 0 + 3 * facet_index] = (Vector4) { x - d        , y            , z     , 0 };
        positions[3 * N_FACETS * i + 1 + 3 * facet_index] = (Vector4) { x - d        , y + d        , z     , 0 };
        positions[3 * N_FACETS * i + 2 + 3 * facet_index] = (Vector4) { x            , y + d / 2.0f , z     , 0 };

        facet_index++;
        positions[3 * N_FACETS * i + 0 + 3 * facet_index] = (Vector4) { x - d        , y            , z     , 0 };
        positions[3 * N_FACETS * i + 1 + 3 * facet_index] = (Vector4) { x - d        , y + d        , z     , 0 };
        positions[3 * N_FACETS * i + 2 + 3 * facet_index] = (Vector4) { x - d / 2.0f , y + d / 2.0f , z + d , 0 };

        facet_index++;
        positions[3 * N_FACETS * i + 0 + 3 * facet_index] = (Vector4) { x - d        , y            , z     , 0 };
        positions[3 * N_FACETS * i + 1 + 3 * facet_index] = (Vector4) { x            , y + d / 2.0f , z     , 0 };
        positions[3 * N_FACETS * i + 2 + 3 * facet_index] = (Vector4) { x - d / 2.0f , y + d / 2.0f , z + d , 0 };

        facet_index++;
        positions[3 * N_FACETS * i + 0 + 3 * facet_index] = (Vector4) { x - d        , y + d        , z     , 0 };
        positions[3 * N_FACETS * i + 1 + 3 * facet_index] = (Vector4) { x - d/ 2.0f  , y + d / 2.0f , z + d , 0 };
        positions[3 * N_FACETS * i + 2 + 3 * facet_index] = (Vector4) { x            , y + d / 2.0f , z     , 0 };

        velocities[i] = (Vector4) { 0.1f, 0.2f * i, 0, 0 };
    }
}


// Compute shader functions
// ------------------------
u32 compute_shader_id;
u32 position_buffer;
u32 velocity_buffer;

void initialize_compute_shader(void) {
    // Load compute shader
    // -------------------
    char *code = LoadFileText("shaders/particle_update_compute.glsl");
    u32 shader_id = rlCompileShader(code, RL_COMPUTE_SHADER);
    if (shader_id == 0) {
        printf("Error: failed to compile compute shader\n");
        exit(1);
    }
    compute_shader_id = rlLoadComputeShaderProgram(shader_id);
    UnloadFileText(code);
    if (compute_shader_id == 0) {
        printf("Error: failed to load compute shader program\n");
        exit(1);
    }

    // Load buffers
    // ------------
    // Transfer data from CPU to GPU
    position_buffer = rlLoadShaderBuffer(3 * N_FACETS * N_PARTICLES * sizeof(Vector4), &positions[0], RL_DYNAMIC_COPY);
    if (position_buffer == 0) {
        printf("Error: failed to load position buffer\n");
        exit(1);
    }

    velocity_buffer = rlLoadShaderBuffer(N_PARTICLES * sizeof(Vector4), &velocities[0], RL_DYNAMIC_COPY);
    if (velocity_buffer == 0) {
        printf("Error: failed to load velocity buffer\n");
        exit(1);
    }


    // Check alignment
    // ---------------
    int align;
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT, &align);
    assert(align == 16); // If this is not true, then the code below that reads from the GPU will probably not work
}


void activate_compute_shader(void) {
    rlEnableShader(compute_shader_id);

    // Set uniforms
    // ------------
    int num_loc = rlGetLocationUniform(compute_shader_id, "numParticles");
    int n_parts = N_PARTICLES;
    rlSetUniform(num_loc, &n_parts, SHADER_UNIFORM_INT, 1);

    int rho_loc = rlGetLocationUniform(compute_shader_id, "rho");
    rlSetUniform(rho_loc, &rho, SHADER_UNIFORM_FLOAT, 1);

    int sigma_loc = rlGetLocationUniform(compute_shader_id, "sigma");
    rlSetUniform(sigma_loc, &sigma, SHADER_UNIFORM_FLOAT, 1);

    int beta_loc = rlGetLocationUniform(compute_shader_id, "beta");
    rlSetUniform(beta_loc, &beta, SHADER_UNIFORM_FLOAT, 1);

    // Activate buffers
    // -----------
    int pos_loc = 0;
    rlBindShaderBuffer(position_buffer, pos_loc);

    int vel_loc = 1;
    rlBindShaderBuffer(velocity_buffer, vel_loc);
}

void deactivate_compute_shader(void) {
    // Deactivate buffers
    // ------------------
    rlBindShaderBuffer(0, 0);
    rlBindShaderBuffer(0, 1);

    rlDisableShader();
}


// Rendering shader functions
// --------------------------
u32 rendering_shader_id;
u32 vao;

void initialize_rendering_shader(void) {
    // Load vertex and fragment shaders
    // --------------------------------
    char *vs_code = LoadFileText("shaders/vert_render_particles.glsl");
    char *fs_code = LoadFileText("shaders/frag_render_particles.glsl");
    u32 vs_id = rlCompileShader(vs_code, RL_VERTEX_SHADER);
    u32 fs_id = rlCompileShader(fs_code, RL_FRAGMENT_SHADER);
    if (vs_id == 0 || fs_id == 0) {
        printf("Error: failed to compile vertex or fragment shader\n");
        exit(1);
    }
    rendering_shader_id = rlLoadShaderProgram(vs_id, fs_id);
    if (rendering_shader_id <= 0) {
        printf("Error: failed to load shader program\n");
        exit(1);
    }
    UnloadFileText(vs_code);
    UnloadFileText(fs_code);


    // Initialize vertex array object
    // ------------------------------
    // The VAO uses the same buffer that the compute shader uses
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao); // OpenGL way of saying "use this VAO"
    glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
    check_error("After buffer stuff");

    // Set up attribute
    // ----------------
    GLuint attribute_index = 0;
    GLint number_of_components_per_attribute = 4;
    GLenum data_type = GL_FLOAT;
    GLboolean should_be_normalized = GL_FALSE;
    GLsizei stride = 4 * sizeof(float); // 0 means tightly packed
    const void *offset = 0;
    glVertexAttribPointer(attribute_index,
                          number_of_components_per_attribute,
                          data_type,
                          should_be_normalized,
                          stride,
                          offset);
    glEnableVertexAttribArray(attribute_index);
    check_error("After attrib pointer");
}



void activate_rendering_shader(Rectangle location) {
    glViewport(location.x, location.y, location.width, location.height);

    glUseProgram(rendering_shader_id);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, position_buffer);
}

void run_rendering_shader(void) {
    glDrawArrays(GL_TRIANGLES, 0, 3 * N_FACETS * N_PARTICLES);
}

void deactivate_rendering_shader(void) {
    glUseProgram(0);
}


void draw_slider(Rectangle location, float *value, float min_value, float max_value, const char *label) {
    DrawRectangleRec(location, GRAY);
    
    Rectangle slider_rect = rect_center(location, 0.9, 0.2);
    slider_rect.y = location.y + 0.10f * location.height;
    DrawRectangleRec(slider_rect, RAYWHITE);

    float progress = (*value - min_value) / (max_value - min_value);
    float center_x = slider_rect.x + progress * slider_rect.width;
    float center_y = slider_rect.y + slider_rect.height / 2.0f;
    float radius = slider_rect.height * 0.8f / 2.0f;
    DrawCircle(center_x, center_y, radius, RED);


    Rectangle label_rect = rect_below_similar(slider_rect, 0.4f);
    DrawText(TextFormat("%s: %0.4f", label, *value), label_rect.x + 10, label_rect.y + label_rect.height / 2.0f, 20, BLACK);

    Vector2 mouse_pos = GetMousePosition();
    if (CheckCollisionPointRec(mouse_pos, slider_rect)) {
        if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
            float mouse_progress = (mouse_pos.x - slider_rect.x) / (slider_rect.width);
            *value = mouse_progress * (max_value - min_value) + min_value;
        }
    }
}


#define W 800
#define H 800 

int main(void) {
    init_particles();
    InitWindow(W, H, "ComputeStart");
    SetTargetFPS(60);

    // Check max work group size
    // -------------------------
    // Could be used to check that we are not trying to simulate too many particles at once
    GLint maxWorkGroupCount[3];
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &maxWorkGroupCount[0]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &maxWorkGroupCount[1]);
    glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &maxWorkGroupCount[2]);
    printf("Max work group count: %d %d %d\n", maxWorkGroupCount[0], maxWorkGroupCount[1], maxWorkGroupCount[2]);


    // Initialize compute shader
    // ---------------------------
    initialize_compute_shader();

    // Initialize rendering shader
    // ---------------------------
    initialize_rendering_shader();
    check_error("After initializing rendering shader");


    // Camera
    // ------
    Camera3D camera = { 0 };
    camera.position = (Vector3) { 0.0f, 0.0f, 2.0f };
    camera.target = (Vector3) { 0.0f, 0.0f, 0.0f };
    camera.up = (Vector3) { 0.0f, 1.0f, 0.0f };
    camera.fovy = 45.0f;
    camera.projection = CAMERA_PERSPECTIVE;


    // DisableCursor();


    // Define rectangles for rendering and for UI
    // ------------------------------------------
    Rectangle window = { 0, 0, W, H};
    Rectangle rendering_rect = rect_center(window, 0.75, 0.75);
    Rectangle sliders_rect = rect_bottom(window, 0.10);
    Rectangle rho_slider = rect_left(sliders_rect, 0.33);
    Rectangle sigma_slider = rect_center(sliders_rect, 0.33, 1.0);
    Rectangle beta_slider = rect_right(sliders_rect, 0.33);


    int screenshot_counter = 0;
    // Main loop
    // ---------
    while (!WindowShouldClose()) {
        Vector2 mouse_pos = GetMousePosition();
        if (CheckCollisionPointRec(mouse_pos, rendering_rect)) {
            UpdateCamera(&camera, CAMERA_FREE);
        }

        BeginDrawing();
        // Clear background
        // ----------------
        ClearBackground((Color) { 51, 51, 51, 255 });
        // ClearBackground((Color) { 75, 75, 150, 255 });
        // ClearBackground((Color) { 94, 74, 54, 255 });
        // ClearBackground((Color) { 51, 44, 37, 255 });
        DrawRectangleRec(rendering_rect, (Color) { 75, 75, 150, 255 });
        EndMode2D();


        // Run equations of motion on GPU
        // ------------------------------
        if (IsKeyReleased(KEY_R) || IsKeyDown(KEY_T)) {
            // Run the compute shader once
            activate_compute_shader();
            int dt_loc = rlGetLocationUniform(compute_shader_id, "dt");
            float dt = GetFrameTime();
            if (dt > max_dt_for_update) {
                dt = max_dt_for_update;
            }
            rlSetUniform(dt_loc, &dt, SHADER_UNIFORM_FLOAT, 1);
            rlComputeShaderDispatch(N_PARTICLES, 1, 1);
            deactivate_compute_shader();
        }

        // Render particles
        // ----------------
        activate_rendering_shader(rendering_rect);
        check_error("After activating rendering");

        // Setup perspective projection
        // ----------------------------
        float aspect = (float)W / (float)H;
        double top = RL_CULL_DISTANCE_NEAR*tan(camera.fovy*0.5*DEG2RAD);
        double right = top*aspect;
        Matrix camera_matrix = GetCameraViewMatrix(&camera);
        camera_matrix = MatrixMultiply(camera_matrix, GetCameraProjectionMatrix(&camera, W / H));
        int camera_loc = rlGetLocationUniform(rendering_shader_id, "camera");
        rlSetUniformMatrix(camera_loc, camera_matrix);
        rlDisableBackfaceCulling();



        // Run rendering shader
        // --------------------
        run_rendering_shader();
        check_error("After running rendering shader");
        deactivate_rendering_shader();
        check_error("After deactivating rendering");


        // Reset viewport
        // --------------
        glViewport(0, 0, W, H);

        // Draw rotation speed
        // -------------------
        if (!recording) {
            DrawText(TextFormat("Angle speed: %f", angle_speed), 10, 10, 20, BLACK);
        }

        // Draw control sliders
        // --------------------
        DrawRectangleRec(sliders_rect, (Color) { 0, 0, 150, 255 });
        draw_slider(rho_slider, &rho, 0.0f, 100.0f, "Rho");
        draw_slider(sigma_slider, &sigma, 0.0f, 100.0f, "sigma");
        draw_slider(beta_slider, &beta, 0.0f, 20.0f, "beta");

        EndDrawing();

        // Handle recording
        // ----------------
        if (recording) {
            time_since_last_frame += GetFrameTime();
            if (time_since_last_frame > recording_speed) {
                time_since_last_frame = 0.0f;
                TakeScreenshot(TextFormat("screenshot_%04i.png", screenshot_counter));
                screenshot_counter++;
            }
        }


        // If orbiting update camera
        // --------------------------
        if (orbit_mode) {
            // Orbit about up axis
            float dt = GetFrameTime();
            Vector3 direction = Vector3Subtract(camera.position, orbit_target);
            Vector3 new_direction = Vector3RotateByQuaternion(direction, QuaternionFromAxisAngle(camera.up, dt * angle_speed));
            camera.position = Vector3Add(orbit_target, new_direction);

            camera.target = orbit_target;
        }


        // Handle input
        // ------------
        {
            if (IsKeyReleased(KEY_Z)) {
                camera.target = (Vector3) { 0.0f, 0.0f, 0.0f };
            }

            // Calculate a good place for the camera to be, based on distribution of particles
            if (IsKeyReleased(KEY_F)) {
                // Focus on center of mass of particles
                //
                //
                // First read particle locations from GPU
                Vector4 *data = malloc(3 * N_FACETS * N_PARTICLES * sizeof(Vector4));
                rlReadShaderBuffer(position_buffer, data, 3 * N_FACETS * N_PARTICLES * sizeof(Vector4), 0);


                // Calculate center of mass
                float avg_x = 0.0f;
                float avg_y = 0.0f;
                float avg_z = 0.0f;
                for (int i = 0; i < N_PARTICLES; i++) {
                    avg_x += data[3 * N_FACETS * i].x;
                    avg_y += data[3 * N_FACETS * i].y;
                    avg_z += data[3 * N_FACETS * i].z;
                }
                avg_x /= N_PARTICLES;
                avg_y /= N_PARTICLES;
                avg_z /= N_PARTICLES;

                // Calculate standard deviation
                float std_x = 0.0f;
                float std_y = 0.0f;
                float std_z = 0.0f;
                for (int i = 0; i < N_PARTICLES; i++) {
                    std_x += (data[3 * N_FACETS * i].x - avg_x) * (data[3 * N_FACETS * i].x - avg_x);
                    std_y += (data[3 * N_FACETS * i].y - avg_y) * (data[3 * N_FACETS * i].y - avg_y);
                    std_z += (data[3 * N_FACETS * i].z - avg_z) * (data[3 * N_FACETS * i].z - avg_z);
                }
                std_x = sqrt(std_x / N_PARTICLES);
                std_y = sqrt(std_y / N_PARTICLES);
                std_z = sqrt(std_z / N_PARTICLES);


                // Make camera point at center of mass and be relatively far away
                camera.target = (Vector3) { avg_x, avg_y, avg_z };
                camera.position = (Vector3) { avg_x, avg_y, avg_z + 4.0f * (std_x + std_y + std_z) };
                free(data);
            }


            // Enable orbit mode
            // -----------------
            if (IsKeyReleased(KEY_O)) {
                orbit_mode = !orbit_mode;
                if (orbit_mode) {
                    orbit_target = camera.target;
                }
            }

            // Go to recording location
            // ------------------------
            // Precalculated and found to be a good spot
            if (IsKeyReleased(KEY_G)) {
                camera.target = (Vector3) {-0.628008, -0.770603, 28.513016};
                camera.position = (Vector3){ -0.628008, -0.770603, 107.093887};
            }

            // Change angle speed
            // ------------------
            if (IsKeyDown(KEY_TWO)) {
                angle_speed += 0.01f;
            } else if (IsKeyDown(KEY_ONE)) {
                angle_speed -= 0.01f;
                if (angle_speed <= 0.0f) {
                    angle_speed = 0.0f;
                }
            } 

            // Toggle recording
            // ----------------
            if (IsKeyReleased(KEY_P)) {
                recording = !recording;
                if (recording) {
                    time_since_last_frame = 0.0f;
                }
            }
        }
    }
    // Deinitialize everything
    // -----------------
    rlUnloadShaderBuffer(position_buffer);
    rlUnloadShaderBuffer(velocity_buffer);
    rlUnloadShaderProgram(compute_shader_id);
    CloseWindow();
    return 0;
}

#!/bin/bash
set -e

INCLUDE="-I./raylib/src"
LIBS="-L./raylib/src -lraylib -ldl -lpthread -lm"
DEFINES="-DGRAPHICS_API_OPENGL_43"
gcc -o main main.c $INCLUDE $LIBS $DEFINES

./main
